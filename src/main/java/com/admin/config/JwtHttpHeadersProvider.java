package com.admin.config;

import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import de.codecentric.boot.admin.server.domain.entities.Instance;
import de.codecentric.boot.admin.server.web.client.HttpHeadersProvider;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class JwtHttpHeadersProvider{// implements HttpHeadersProvider {

	@Value("${jwt.secret}")
	private String secret;
//	
//	@Override
//	public HttpHeaders getHeaders(Instance instance) {
//		// TODO Auto-generated method stub
//		HttpHeaders headers = new HttpHeaders();
//		headers.set("Authorization","Bearer "+doGenerateToken());
//		return headers;
//	}

	
	private String doGenerateToken() {
		Base64.Decoder decoder = Base64.getDecoder();
		Map<String, Object> claims = new HashMap<>();
		return Jwts.builder().setClaims(claims).setSubject("SBAadmin")
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.signWith(SignatureAlgorithm.HS512, decoder.decode(secret)).compact();
	}

}
